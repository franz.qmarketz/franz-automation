# Automart Automation

## List of Playwright Commands
```
npx playwright test --browser=all \\run test to all browser
npx playwright test \\only run for the test not test all the browser
npx playwright show-report \\test automation will create index.html so you can check the result on the hmtl report

npx playwright codegen
```


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jammel.qmarketz/automart-automation.git
git branch -M main
git push -uf origin main
```
 
