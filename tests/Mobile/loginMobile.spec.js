import { test, expect } from "@playwright/test";
import { _android as android } from "playwright";

test("Enter Invalid Email Address & valid Password", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });
    console.log(await page.title());

    //Click Email Address textbox
    await page.locator('[placeholder="Email Address \\*"]').click();

    //Enter Email Address
    await page
      .locator('[placeholder="Email Address \\*"]')
      .fill("testsample@gmail.com");

    //Click Pasword textbox
    await page.locator('[placeholder="Password \\*"]').click();

    //Enter Password
    await page.locator('[placeholder="Password \\*"]').fill("qwe");

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
    const errorMessage = page.locator("text=Invalid credentials");
    await expect(errorMessage).toContainText("Invalid credentials");
  }
});

test("Enter Valid Email Address & Invalid Password", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });
    console.log(await page.title());

    //Click Email Address textbox
    await page.locator('[placeholder="Email Address \\*"]').click();

    //Enter Email Address
    await page
      .locator('[placeholder="Email Address \\*"]')
      .fill("testsample@gmail.com");

    //Click Pasword textbox
    await page.locator('[placeholder="Password \\*"]').click();

    //Enter Password
    await page.locator('[placeholder="Password \\*"]').fill("qwe");

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
    const errorMessage = page.locator("text=Invalid credentials");
    await expect(errorMessage).toContainText("Invalid credentials");
  }
});

test("Enter Invalid Email Address & Invalid Password", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });
    //Click Email Address textbox
    await page.locator('[placeholder="Email Address \\*"]').click();

    //Enter Email Address
    await page
      .locator('[placeholder="Email Address \\*"]')
      .fill("testsample@gmail.com");

    //Click Pasword textbox
    await page.locator('[placeholder="Password \\*"]').click();

    //Enter Password
    await page.locator('[placeholder="Password \\*"]').fill("qwe");

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
    const errorMessage = page.locator("text=Invalid credentials");
    await expect(errorMessage).toContainText("Invalid credentials");
  }
});

test("Enter Null Email Address & Null Password", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
    const errorMessage = page.locator(
      "text=email format is invalid ,email should not be empty,password should not be empty"
    );
    await expect(errorMessage).toContainText(
      "email format is invalid ,email should not be empty,password should not be empty"
    );
  }
});

test("Enter Null Email Address & Valid Password", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });

    //Click Password Textbox
    await page.locator('[placeholder="Password \\*"]').click();

    //Enter Password
    await page.locator('[placeholder="Password \\*"]').fill("qwe");

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should be able to show Error Message "Email format is invalid"
    const errorMessage = page.locator(
      "text=email format is invalid ,email should not be empty"
    );
    await expect(errorMessage).toContainText(
      "email format is invalid ,email should not be empty"
    );
  }
});

test("Enter Valid Email Address & Null Password", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });

    //Click email address textbox
    await page.locator('[placeholder="Email Address \\*"]').click();

    //Enter Email address
    await page
      .locator('[placeholder="Email Address \\*"]')
      .fill("testv5@gmail.com");

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should be able to show Error Message "Email format is invalid"
    const errorMessage = page.locator("text=password should not be empty");
    await expect(errorMessage).toContainText("password should not be empty");
  }
});

test("Login using facebook", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });
    console.log(await page.title());

    // Click Facebook
    await page.click("#facebook-login-button");

    //Assertion
    //Expected Result: Should be able to click login button and show pop up page
    const facebook = page.locator("#facebook-login-button");
    await expect(facebook).toHaveText("Facebook");
  }
});

test("Login using Google Account", async ({ page }) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });

    //click google button
    await page.click("#google-login-button");

    //Assertion
    //Expected Result: Should be able to click login button and show pop up page
    const google = page.locator("#google-login-button");
    await expect(google).toHaveText("Google");
  }
});

test("Enter valid Email Address & valid Password", async ({}) => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Take screenshot of the whole device.
  await device.screenshot({ path: "device.png" });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell("am force-stop org.chromium.webview_shell");
    await device.shell(
      "am start org.chromium.webview_shell/.WebViewBrowserActivity"
    );
    // Get the WebView.
    const webview = await device.webView({ pkg: "org.chromium.webview_shell" });

    // Fill the input box.
    await device.fill(
      { res: "org.chromium.webview_shell:id/url_field" },
      "https://automart-staging-v5.vercel.app/login"
    );
    await device.press(
      { res: "org.chromium.webview_shell:id/url_field" },
      "Enter"
    );

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({
      url: "https://automart-staging-v5.vercel.app/login",
    });

    //Email Address
    await page.locator('[placeholder="Email Address \\*"]').click();

    //Enter Email Address
    await page
      .locator('[placeholder="Email Address \\*"]')
      .fill("testv5@gmail.com");

    //Click Pasword textbox
    await page.locator('[placeholder="Password \\*"]').click();

    //Enter Password
    await page.locator('[placeholder="Password \\*"]').fill("Superpw64");

    //Click Submit button
    await page.locator("text=SUBMIT").click();

    //Assertion
    //Expected Result: Should redirect to Home page
    await expect(page).toHaveURL("https://automart-staging-v5.vercel.app/");
  }
});
