
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'
//var randomstring = require("randomstring")


test("Click Certified Cars in the Footer should redirect to certified cars page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

  //view All Cars
  await page.waitForSelector('#footer-link-certified-cars')
  await page.click('#footer-link-certified-cars')
  
  // assertion
  const footerCertifiedCars = await page.locator('#footer-link-certified-cars').count()
  
  // verify assertion
  expect(footerCertifiedCars).toBeDefined()
}); 

test("Click Used Cars for Sale in the Footer should redirect to All Used cars page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //view used cars
  await page.waitForSelector('#footer-link-used-cars')
  await page.click('#footer-link-used-cars')
  
  // assertion
  const footerUsedCars = await page.locator('#footer-link-used-cars').count()
  
  // verify assertion
  expect(footerUsedCars).toBeDefined()
}); 

test("Click Repossessed Cars in the Footer should redirect to All Used cars page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //view Repossessed Cars
  await page.waitForSelector('#footer-link-repossessed-cars')
  await page.click('#footer-link-repossessed-cars')
  
  // assertion
  const footerRepossessedCars = await page.locator('#footer-link-repossessed-cars').count()
  
  // verify assertion
  expect(footerRepossessedCars).toBeDefined()
}); 

test("Click Sell My Car in the Footer should redirect to Sell or Trade-in cars page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //sell my car Cars
  await page.waitForSelector('#footer-link-sell-my-car')
  await page.click('#footer-link-sell-my-car')
  
  // assertion
  const footerSellMyCars = await page.locator('#footer-link-sell-my-car').count()
  
  // verify assertion
  expect(footerSellMyCars).toBeDefined()
}); 

test("Click Car Insurance in the Footer should redirect to Assurance.ph page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //Car Insurance
await page.waitForSelector('#footer-link-eastwest-bank')
await page.click('#footer-link-eastwest-bank')
  
  // assertion
  const footerCarInsurance = await page.locator('#footer-link-car-insurance').count()
  
  // verify assertion
  expect(footerCarInsurance).toBeDefined()
}); 

test("Click Eastwest Bank in Footer should redirect to Eastwest Bank page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //Eastwest bank
  await page.waitForSelector('#footer-link-eastwest-bank')
  await page.click('#footer-link-eastwest-bank')
  
  // assertion
  const footerEW = await page.locator('#footer-link-eastwest-bank').count()
  
  // verify assertion
  expect(footerEW).toBeDefined()
}); 

test("Click BDO in Footer should redirect to BDO Bank page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //BDO bank
  await page.waitForSelector('#footer-link-bdo')
  await page.click('#footer-link-bdo')
  
  // assertion
  const footerBDO = await page.locator('#footer-link-bdo').count()
  
  // verify assertion
  expect(footerBDO).toBeDefined()
}); 

test("Click Security Bank in Footer should redirect to Security Bank page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //Security bank
  await page.waitForSelector('#footer-link-security-bank')
  await page.click('#footer-link-security-bank')
  
  // assertion
  const footerSB = await page.locator('#footer-link-security-bank').count()
  
  // verify assertion
  expect(footerSB).toBeDefined()
}); 

test("Click Orix in Footer should redirect to Orix page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //Orix
  await page.waitForSelector('#footer-link-orix')
  await page.click('#footer-link-orix')
  
  // assertion
  const footerOrix = await page.locator('#footer-link-orix').count()
  
  // verify assertion
  expect(footerOrix).toBeDefined()
}); 


test("Click TFS in Footer should redirect to TFS page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //TFS
  await page.waitForSelector('#footer-link-tfs')
  await page.click('#footer-link-tfs')
  
  // assertion
  const footerTFS = await page.locator('#footer-link-tfs').count()
  
  // verify assertion
  expect(footerTFS).toBeDefined()
}); 



test("Click Buena Mano in Footer should redirect to Buena Mano page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
 
  
    
  //Buena Mano
  await page.waitForSelector('#footer-link-buena-mano')
  await page.click('#footer-link-buena-mano')
  
  // assertion
  const footerBM = await page.locator('#footer-link-buena-mano').count()
  
  // verify assertion
  expect(footerBM).toBeDefined()
}); 