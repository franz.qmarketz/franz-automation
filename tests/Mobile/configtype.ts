import * as fs from 'fs'
import * as toml from 'toml'

const config = toml.parse(fs.readFileSync('./config.toml', 'utf-8'));

declare global {
}

export default {
  AUTOMART_URL: config.automart_url ?? '',
  };