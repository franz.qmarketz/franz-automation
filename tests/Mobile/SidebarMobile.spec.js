
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'

test("open sidebar ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  
  // assertion
  const sidebar = await page.locator('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root').count()

  // verify if go to signup modal
  expect(sidebar).toBeDefined()
}); 



test("open sidebar click login", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //login
  await page.waitForSelector('.MuiPaper-root > .MuiBox-root > #sidebar-login > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiBox-root > #sidebar-login > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const login = await page.locator('.MuiPaper-root > .MuiBox-root > #sidebar-login > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(login).toBeDefined()
}); 


test("open sidebar click Register ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Register
  await page.waitForSelector('.MuiPaper-root > .MuiBox-root > #sidebar-register > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiBox-root > #sidebar-register > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const register = await page.locator('.MuiPaper-root > .MuiBox-root > #sidebar-register > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(register).toBeDefined()
}); 


test("open sidebar Click Home ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Home
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-home > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-home > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const Home = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-home > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(Home).toBeDefined()
}); 


test("open sidebar click FAQs ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //FAQS
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-faqs > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-faqs > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const FAQs = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-faqs > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(FAQs).toBeDefined()
}); 


test("open sidebar click Reviews", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Reviews
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-reviews > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-reviews > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const reviews = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-reviews > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(reviews).toBeDefined()
}); 


test("open sidebar click Blogs ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Blogs
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-blogs > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-blogs > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const blogs = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-blogs > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(blogs).toBeDefined()
}); 


test("open sidebar click Certified Used Cars ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Certified used Cars
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-certified-used-cars > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-certified-used-cars > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const ACUV = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-certified-used-cars > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(ACUV).toBeDefined()
}); 


test("open sidebar click Sell My Car", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Sell my car
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-sell-my-car > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-sell-my-car > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const sellmycar = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-sell-my-car > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(sellmycar).toBeDefined()
}); 


test("open sidebar click Automart", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Automart
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-assetmart > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-assetmart > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const assetmart = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-assetmart > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(assetmart).toBeDefined()
}); 


test("open sidebar click Assurance ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')

  //Assurance
  await page.waitForSelector('.MuiPaper-root > .MuiList-root > #sidebar-assurance > .MuiListItemText-root > .MuiTypography-root')
  await page.click('.MuiPaper-root > .MuiList-root > #sidebar-assurance > .MuiListItemText-root > .MuiTypography-root')
  
  // assertion
  const sidebar = await page.locator('.MuiPaper-root > .MuiList-root > #sidebar-assurance > .MuiListItemText-root > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(sidebar).toBeDefined()
}); 


test("close sidebar ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //sidebar button
  await page.waitForSelector('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  await page.click('#__next > .MuiPaper-root > .MuiToolbar-root > #menu-navbar > .MuiSvgIcon-root')
  
  await page.waitForSelector('#sidebar-close')
  await page.click('#sidebar-close')
  // assertion
  const closesidebar = await page.locator('#sidebar-close').count()

  // verify if go to signup modal
  expect(closesidebar).toBeDefined()
}); 
