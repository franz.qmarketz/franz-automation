
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'


test("Open search dropdown ", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    

    //search dropdown
    await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
    await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

    // assertion
    const dropdownsearch = await page.locator('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root').count()
  
    // verify if dropdown search show
    expect(dropdownsearch).toBeDefined()
  }); 



  test("search car by popular searches ", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    

    //search dropdown
    await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
    await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

    await page.waitForSelector('.MuiContainer-root > .MuiBox-root > .MuiBox-root > .MuiButtonBase-root:nth-child(1) > .MuiChip-label')
    await page.click('.MuiContainer-root > .MuiBox-root > .MuiBox-root > .MuiButtonBase-root:nth-child(1) > .MuiChip-label')

    // assertion
    const popularsearch = await page.locator('.MuiContainer-root > .MuiBox-root > .MuiBox-root > .MuiButtonBase-root:nth-child(1) > .MuiChip-label').count()
  
  
    expect(popularsearch).toBeDefined()
  }); 


  
test("search car by location", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  
  //Location tab
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-location > .MuiBadge-root > .MuiSvgIcon-root')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-location > .MuiBadge-root > .MuiSvgIcon-root')

  //Quezon City
  await page.waitForSelector('.MuiList-root > div > #list-item-3 > #checkbox-list-secondary-label-quezon-city > .MuiTypography-root')
  await page.click('.MuiList-root > div > #list-item-3 > #checkbox-list-secondary-label-quezon-city > .MuiTypography-root')
  
  //kalayaan
  await page.waitForSelector('.MuiList-root > .MuiListItem-container > #list-item-97 > #checkbox-list-label-97 > .MuiTypography-root')
  await page.click('.MuiList-root > .MuiListItem-container > #list-item-97 > #checkbox-list-label-97 > .MuiTypography-root')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')

  // assertion
  const searchlocation = await page.locator('#filter-submit').count()


  expect(searchlocation).toBeDefined()
}); 


test("search car by car type ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  
  //car type
  await page.waitForSelector('#filter-car-type')
  await page.click('#filter-car-type')
  
  //SUV
  await page.waitForSelector('div:nth-child(4) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(4) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  
  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const cartype = await page.locator('#filter-submit').count()


  expect(cartype).toBeDefined()
}); 


test("search car by brand ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
 
  //car brand
  await page.waitForSelector('#filter-brand')
  await page.click('#filter-brand')
  
  await page.waitForSelector('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')


  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const carbrand = await page.locator('#filter-submit').count()



  expect(carbrand).toBeDefined()
}); 

test("search car by Price ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //Price
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-price > .MuiBadge-root > .mdi-icon')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-price > .MuiBadge-root > .mdi-icon')

  await page.waitForSelector('#simple-tabpanel-3 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')
  await page.click('#simple-tabpanel-3 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const carprice = await page.locator('#filter-submit').count()


  expect(carprice).toBeDefined()
}); 


test("search car by Year Model ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //year model
  await page.waitForSelector('#filter-year-model')
  await page.click('#filter-year-model')

  await page.waitForSelector('#simple-tabpanel-4 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')
  await page.click('#simple-tabpanel-4 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit') 
  // assertion
  const yearmodel = await page.locator('#filter-submit') .count()


  expect(yearmodel).toBeDefined()
}); 

test("search car by Transmission ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //transmission
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-transmission > .MuiBadge-root > .mdi-icon')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-transmission > .MuiBadge-root > .mdi-icon')
  
  await page.waitForSelector('div:nth-child(1) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(1) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  
  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const transmission = await page.locator('#filter-submit').count()


  expect(transmission).toBeDefined()
}); 

test("search car by Sale Type ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
 

 //search dropdown
 await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
 await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

 //Sale Type
 await page.waitForSelector('.MuiTabs-flexContainer > #filter-sale-type > .MuiBadge-root > .MuiSvgIcon-root > path')
 await page.click('.MuiTabs-flexContainer > #filter-sale-type > .MuiBadge-root > .MuiSvgIcon-root > path')
 
 await page.waitForSelector('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
 await page.click('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
 
 await page.waitForSelector('#filter-submit')
 await page.click('#filter-submit')
 // assertion
 const salestype = await page.locator('#filter-submit').count()


 expect(salestype).toBeDefined()

}); 


test("search car by Odometer ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));


 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
 
  //odometer
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-odometer > .MuiBadge-root > .MuiSvgIcon-root')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-odometer > .MuiBadge-root > .MuiSvgIcon-root')
  
  await page.waitForSelector('#simple-tabpanel-7 > .MuiBox-root > .MuiBox-root > #odometer-range-10k-below > .MuiChip-label')
  await page.click('#simple-tabpanel-7 > .MuiBox-root > .MuiBox-root > #odometer-range-10k-below > .MuiChip-label')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const odometer = await page.locator('#filter-submit').count()


  expect(odometer).toBeDefined()
}); 

test("search car by Plate Ending ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));


 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
 
  //Plate ending

  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-plate-ending > .MuiBadge-root > .MuiSvgIcon-root')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-plate-ending > .MuiBadge-root > .MuiSvgIcon-root')
  
  await page.waitForSelector('.MuiList-root > .MuiListItem-container:nth-child(1) > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('.MuiList-root > .MuiListItem-container:nth-child(1) > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const plateending = await page.locator('#filter-submit').count()


  expect(plateending).toBeDefined()
}); 

test("Reset Search ", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));


 
  

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

//reset button
await page.waitForSelector('#filter-reset')
await page.click('#filter-reset')
  // assertion
  const resetbutton = await page.locator('#filter-reset').count()

  expect(resetbutton).toBeDefined()
}); 