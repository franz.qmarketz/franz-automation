import { test, expect } from '@playwright/test'

test("Select General Trias Cavite Top Warehouses should redirect to Gen Trias Page", async ({ page }) => {

    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
  
    //Our Locations
    await page.waitForSelector('body > #__next > .mt-20 > .my-20 > .mb-8')
    await page.click('body > #__next > .mt-20 > .my-20 > .mb-8')
  
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Gen Trias
    await page.waitForSelector('.mt-20 > .my-20 > .mt-10 > #top-warehouses-gen-trias > .w-fit:nth-child(1)')
    await page.click('.mt-20 > .my-20 > .mt-10 > #top-warehouses-gen-trias > .w-fit:nth-child(1)')
  
    // assertion
    const GenTrias = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy').count()
  
    // verify assertion
    expect(GenTrias).toBeDefined()
  });