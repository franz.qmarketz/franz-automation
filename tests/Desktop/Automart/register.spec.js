import { test, expect } from "@playwright/test";
var randomstring = require("randomstring");

test("Register without email address", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see error message
  const noemail = page.locator("text=EmailThis field is required");
  await expect(noemail).toHaveText("EmailThis field is required");
});

test("Register without Password", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see error message that password field is required
  const noPassword = page.locator("text=PasswordThis field is required");
  await expect(noPassword).toHaveText("PasswordThis field is required");
});

test("Register without confirm Password", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see Error message that confirm password is required
  const noConfirmPassword = page.locator(
    "text=Confirm PasswordThis field is required"
  );
  await expect(noConfirmPassword).toHaveText(
    "Confirm PasswordThis field is required"
  );
});

test("Register without first name", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see Error message that First name is required
  const noFirstName = page.locator("text=First NameThis field is required");
  await expect(noFirstName).toHaveText("First NameThis field is required");
});

test("Register without middle name", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see error message that required Middle name or type N/A
  const noMiddleName = page.locator(
    'xpath=//*[@id="registration-form"]/div/div[4]/div[2]/p[2]'
  );
  await expect(noMiddleName).toBeVisible();
});

test("Register without last name", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see Error message that required last name
  const noLastName = page.locator("text=Last NameThis field is required");
  await expect(noLastName).toHaveText("Last NameThis field is required");
});

test("Register without contact number", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see error message that require contact number
  const noContactNumber = page.locator("text=Contact NumberThis field is required");
  await expect(noContactNumber).toHaveText("Contact NumberThis field is required");
});

test("Register Successfully", async ({ page }) => {
  // Go to the Automart carkuya page web page

  await page.goto("https://automart-staging-v5.vercel.app/register");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to click signup button and redirect to welcome page
  const signup = page.locator("#sign-up-registration-button");
  await expect(signup).toHaveText("SIGN UP");
});
