import { test, expect } from '@playwright/test'

test("open sidebar ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  const drawer = page.locator('[aria-label="Open drawer"]')

  //Assertion
  //Expected Result: Should redirect to login page
  await expect(drawer).toBeVisible();
});



test("open sidebar click login", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Login button
  await page.locator('text=Login').click();

  //Assertion
  //Expected Result: Should redirect to login page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/login');
});


test("open sidebar click Register ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Register
  await page.locator('button:has-text("Register")').click();

  //Assertion
  //Expected Result: Should redirect to Register page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/register');
});


test("open sidebar Click Home ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Home
  await page.locator('text=Home').click();

  // assertion
  //Expected Result: Should redirect to certified cars page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/');
});


test("open sidebar click FAQs ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click FAQS
  await page.locator('button:has-text("FAQs")').click();

  // assertion
  //Expected Result: Should redirect to certified cars page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/blog/frequently-asked-questions');
});


test("open sidebar click Reviews", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Reviews
  await page.locator('button:has-text("Reviews")').click();

  // assertion
  //Expected Result: Should redirect to Reviews page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/reviews');
});


test("open sidebar click Blogs ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Blogs
  await page.locator('button:has-text("Blogs")').click();

  // assertion
  //Expected Result: Should redirect to blogs page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/blog');
});


test("open sidebar click Certified Used Cars ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Certified used Cars
  await page.locator('button:has-text("Certified Cars")').click();

  // assertion
  //Expected Result: Should redirect to certified cars page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/certified-cars');
});


test("open sidebar click Sell My Car", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Sell my car
  await page.locator('button:has-text("Sell My Car")').click();

  // assertion
  //Expected Result: Should redirect to Sell my car page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/sell-my-car');
});


test("open sidebar click Assetmart", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Assetmart
  await page.locator('button:has-text("Assetmart")').click();

  //Assertion
  //Expected Result: Should redirect to Assetmart Page
  await expect(page).toHaveURL('https://assetmart.global/');
});


test("open sidebar click Assurance ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar button
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Assurance
  await page.locator('button:has-text("Assurance")').click();

  //Assertion
  //Expected Result: Should redirect to Assurance page
  await expect(page).toHaveURL('https://assurance.ph/');
});


test("close sidebar ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Sidebar
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Sidebar close button
  const closeSidebar = page.locator('[id="__next"] > div > div > .icon-ripple')

  //Assertion
  //Expected Result: Should redirect to login page
  await expect(closeSidebar).toBeVisible();
}); 
