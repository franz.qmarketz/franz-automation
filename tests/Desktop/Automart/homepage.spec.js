import { test, expect } from '@playwright/test'

test("View Automart.ph Homepage", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  // Go to https://automart-staging-v5.vercel.app/
  await page.goto('https://automart-staging-v5.vercel.app/');


  //assertion
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app');

});
test("Click Watchlist Heart button", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Heart button
  await page.waitForSelector('.ml-auto > a > .icon-ripple > .mdi-icon > path')
  await page.click('.ml-auto > a > .icon-ripple > .mdi-icon > path')

  // assertion
  const Heartbutton = await page.locator('.MuiToolbar-root > .css-18j51vt > #favorite-button-navbar > .MuiSvgIcon-root > path').count()

  // verify assertion
  expect(Heartbutton).toBeDefined()
});

test(" Click Contact Number in Navbar", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Contact number
  await page.waitForSelector('.fixed > .bg-gray > .ml-auto > a:nth-child(2) > .bg-transparent')
  await page.click('.fixed > .bg-gray > .ml-auto > a:nth-child(2) > .bg-transparent')

  // assertion
  const telephonenavbar = await page.locator('.fixed > .bg-gray > .ml-auto > a:nth-child(2) > .bg-transparent').count()

  // verify assertion
  expect(telephonenavbar).toBeDefined()
});

test(" Click New Cars in Navbar should redirect to new cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //New Cars
  await page.waitForSelector('.scrollbar-hide > #new-cars-subnavbar > .flex > svg > path:nth-child(2)')
  await page.click('.scrollbar-hide > #new-cars-subnavbar > .flex > svg > path:nth-child(2)')

  // assertion
  const newcarsnavbar = await page.locator('.scrollbar-hide > #new-cars-subnavbar > .flex > svg > path:nth-child(2)').count()

  // verify assertion
  expect(newcarsnavbar).toBeDefined()
});

test(" Click Certified Cars in Navbar should redirect to certified used cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Certified Cars
  await page.waitForSelector('.relative > .scrollbar-hide > #certified-cars-subnavbar > .flex > svg')
  await page.click('.relative > .scrollbar-hide > #certified-cars-subnavbar > .flex > svg')

  // assertion
  const certfiedcarsnavbar = await page.locator('.relative > .scrollbar-hide > #certified-cars-subnavbar > .flex > svg').count()

  // verify assertion
  expect(certfiedcarsnavbar).toBeDefined()
});

test(" Click Used Cars in Navbar should redirect to All used cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Used Cars
  await page.waitForSelector('#used-cars-subnavbar')
  await page.click('#used-cars-subnavbar')

  // assertion
  const usedcarsnavbar = await page.locator('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #used-cars-subnavbar > svg').count()

  // verify assertion
  expect(usedcarsnavbar).toBeDefined()
});

test(" Click Sell My Car in Navbar should redirect to Sell My Car Page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Sell My Car
  await page.waitForSelector('#sell-my-car-subnavbar')
  await page.click('#sell-my-car-subnavbar')

  // assertion
  const sellmycarnavbar = await page.locator('#sell-my-car-subnavbar').count()

  // verify assertion
  expect(sellmycarnavbar).toBeDefined()
});

test(" Click Reviews in Navbar should redirect to Reviews page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //reviews
  await page.waitForSelector('#reviews-subnavbar')
  await page.click('#reviews-subnavbar')
  // assertion
  const reviewsnavbar = await page.locator('#reviews-subnavbar').count()

  // verify assertion
  expect(reviewsnavbar).toBeDefined()
});

test(" Click Blogs in Navbar should redirect to Blogs page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Blogs
  await page.waitForSelector('.relative > .scrollbar-hide > #blogs-subnavbar > .flex > svg')
  await page.click('.relative > .scrollbar-hide > #blogs-subnavbar > .flex > svg')

  // assertion
  const blogsnavbar = await page.locator('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #blogs-subnavbar > svg').count()

  // verify assertion
  expect(blogsnavbar).toBeDefined()
});

test("Click Buy button in Homepage should redirect to All used cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Buy button
  await page.waitForSelector('#hero-button-buy')
  await page.click('#hero-button-buy')

  // assertion
  const Buybutton = await page.locator('#hero-button-buy').count()

  // verify assertion
  expect(Buybutton).toBeDefined()
});

test("Click Sell or Trade-In button in Homepage should redirect to Sell My Car Page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Sell or Trade-In button
  await page.waitForSelector('#hero-button-sell')
  await page.click('#hero-button-sell')

  // assertion
  const Tradeinbutton = await page.locator('#hero-button-sell').count()

  // verify assertion
  expect(Tradeinbutton).toBeDefined()
});

test("Click Insurance button in Homepage should redirect to Assurance Page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Insurance button
  await page.waitForSelector('#hero-button-insurance')
  await page.click('#hero-button-insurance')
  // assertion
  const Insurancebutton = await page.locator('#hero-button-insurance').count()

  // verify assertion
  expect(Insurancebutton).toBeDefined()
});

test("Click Browse all Cars button should redirect to All used cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Insurance button
  await page.waitForSelector('#hero-button-browse')
  await page.click('#hero-button-browse')
  // assertion
  const Browseallcars = await page.locator('#hero-button-browse').count()

  // verify assertion
  expect(Browseallcars).toBeDefined()
});

test("Click See All ACV button should redirect to Certified Cars page ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //view automart certified vehicle
  await page.waitForSelector('#button-see-all-acv')
  await page.click('#button-see-all-acv')
  // assertion
  const allACVbutton = await page.locator('#button-see-all-acv').count()

  // verify assertion
  expect(allACVbutton).toBeDefined()
});

test("Contact Us using viber", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //contact us using viber
  await page.waitForSelector('.relative > .mt-8 > span:nth-child(1) > .flex > .m-3')
  await page.click('.relative > .mt-8 > span:nth-child(1) > .flex > .m-3')


  // assertion
  const viber = await page.locator('.relative > .mt-8 > span:nth-child(1) > .flex > .m-3').count()

  // verify assertion
  expect(viber).toBeDefined()
});

test("Contact Us using landline", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //contact us using landline
  await page.waitForSelector('.relative > .mt-8 > span:nth-child(2) > .flex > .m-3')
  await page.click('.relative > .mt-8 > span:nth-child(2) > .flex > .m-3')


  // assertion
  const landline = await page.locator('.relative > .mt-8 > span:nth-child(2) > .flex > .m-3').count()

  // verify assertion
  expect(landline).toBeDefined()
});

test("Contact Us using email", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //contact us using email
  await page.waitForSelector('.relative > .mt-8 > span:nth-child(3) > .flex > .m-3')
  await page.click('.relative > .mt-8 > span:nth-child(3) > .flex > .m-3')


  // assertion
  const email = await page.locator('.relative > .mt-8 > span:nth-child(3) > .flex > .m-3').count()

  // verify assertion
  expect(email).toBeDefined()
});

test("Contact Us using messenger", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //contact us using messenger
  await page.waitForSelector('.relative > .mt-8 > span:nth-child(4) > .flex > .m-3')
  await page.click('.relative > .mt-8 > span:nth-child(4) > .flex > .m-3')


  // assertion
  const messenger = await page.locator('.relative > .mt-8 > span:nth-child(4) > .flex > .m-3').count()

  // verify assertion
  expect(messenger).toBeDefined()
});

test("View Popular Vehicles by Model", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Model
  await page.waitForSelector('#tab-popular-vehicles-by-model')
  await page.click('#tab-popular-vehicles-by-model')

  // assertion
  const model = await page.locator('#tab-popular-vehicles-by-model').count()

  // verify assertion
  expect(model).toBeDefined()
});

test("Select Toyota Model in Popular vehicles should redirect to Toyota Model Page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Model
  await page.waitForSelector('#tab-popular-vehicles-by-model')
  await page.click('#tab-popular-vehicles-by-model')

  //Select vios
  await page.waitForSelector('.flex > .flex-shrink-0 > #lp-card-toyota-vios > span > img')
  await page.click('.flex > .flex-shrink-0 > #lp-card-toyota-vios > span > img')

  // assertion
  const toyotaModel = await page.locator('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota-vios > .MuiBox-root > .MuiTypography-root:nth-child(1)').count()

  // verify assertion
  expect(toyotaModel).toBeDefined()
});

test("Click See All Car Models button should redirect to All Models page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Model
  await page.waitForSelector('#tab-popular-vehicles-by-model')
  await page.click('#tab-popular-vehicles-by-model')

  //see all car model button
  await page.waitForSelector('#popular-see-all-models-button')
  await page.click('#popular-see-all-models-button')

  // assertion
  const SeeAllCarModel = await page.locator('#popular-see-all-models-button').count()

  // verify assertion
  expect(SeeAllCarModel).toBeDefined()
});

test("View Popular Vehicles by Body Type", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Body type
  await page.waitForSelector('#tab-popular-vehicles-by-body-type')
  await page.click('#tab-popular-vehicles-by-body-type')

  // assertion
  const bodytype = await page.locator('#tab-popular-vehicles-by-body-type').count()

  // verify assertion
  expect(bodytype).toBeDefined()
});

test("Select Sedan Body Type should redirect to Sedan page(should show all the sedan type vehicles) ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Body type
  await page.waitForSelector('#tab-popular-vehicles-by-body-type')
  await page.click('#tab-popular-vehicles-by-body-type')

  //sedan
  await page.waitForSelector('.flex > .flex-shrink-0 > #lp-card-sedan > span > img')
  await page.click('.flex > .flex-shrink-0 > #lp-card-sedan > span > img')

  // assertion
  const sedanbodyType = await page.locator('.MuiGrid-root > .MuiGrid-root > #lp-card-sedan > .MuiBox-root > .MuiTypography-root:nth-child(1)').count()

  // verify assertion
  expect(sedanbodyType).toBeDefined()
});

test("View Popular Vehicles by Brand", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Brand
  await page.waitForSelector('#tab-popular-vehicles-by-brand')
  await page.click('#tab-popular-vehicles-by-brand')

  // assertion
  const brand = await page.locator('#tab-popular-vehicles-by-brand').count()

  // verify assertion
  expect(brand).toBeDefined()
});

test("Select Toyota Brand Should redirect to Toyota Page ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Brand
  await page.waitForSelector('#tab-popular-vehicles-by-brand')
  await page.click('#tab-popular-vehicles-by-brand')

  //Toyota
  await page.waitForSelector('.flex > .flex-shrink-0 > #lp-card-toyota > span > img')
  await page.click('.flex > .flex-shrink-0 > #lp-card-toyota > span > img')

  // assertion
  const ToyotaBrand = await page.locator('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota > .MuiBox-root > .MuiTypography-root:nth-child(1)').count()

  // verify assertion
  expect(ToyotaBrand).toBeDefined()
});

test("Click See All Car Brands button should redirect to All Brands page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Popular vehicles by
  await page.waitForSelector('body > #__next > .relative > .mx-auto > .mb-10')
  await page.click('body > #__next > .relative > .mx-auto > .mb-10')

  //Brand
  await page.waitForSelector('#tab-popular-vehicles-by-brand')
  await page.click('#tab-popular-vehicles-by-brand')

  //see all car brands button
  await page.waitForSelector('#popular-see-all-brands-button')
  await page.click('#popular-see-all-brands-button')

  // assertion
  const SeeAllCarBrand = await page.locator('#popular-see-all-brands-button').count()

  // verify assertion
  expect(SeeAllCarBrand).toBeDefined()
});

test("3 simple steps to get your car -> find the right car", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //find the right car
  await page.waitForSelector('#simple-steps-browse-all-cars')
  await page.click('#simple-steps-browse-all-cars')


  // assertion
  const findrightcar = await page.locator('#simple-steps-browse-all-cars').count()

  // verify assertion
  expect(findrightcar).toBeDefined()
});

test("3 simple steps to get your car -> connect to an adviser", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //connecdt to an  adviser
  await page.waitForSelector('#simple-steps-connect-to-an-adviser')
  await page.click('#simple-steps-connect-to-an-adviser')


  // assertion
  const connectadviser = await page.locator('#simple-steps-connect-to-an-adviser').count()

  // verify assertion
  expect(connectadviser).toBeDefined()
});



test("3 simple steps to get your car -> Buy immediately or bid on a car", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Buy immediately or bid on a car
  await page.waitForSelector('#simple-steps-learn-more')
  await page.click('#simple-steps-learn-more')

  // assertion
  const learnmore = await page.locator('#simple-steps-learn-more').count()

  // verify assertion
  expect(learnmore).toBeDefined()
});

test("Sell / Trade-in your car", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //get quotation button
  await page.waitForSelector('#can-do-get-quotation')
  await page.click('#can-do-get-quotation')


  // assertion
  const getquotationbutton = await page.locator('#can-do-get-quotation').count()

  // verify assertion
  expect(getquotationbutton).toBeDefined()
});

test("Get car insurance", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //connect to an adviser button
  await page.waitForSelector('#can-do-connect-to-an-adviser')
  await page.click('#can-do-connect-to-an-adviser')


  // assertion
  const connecttoanadviserbutton = await page.locator('#can-do-connect-to-an-adviser').count()

  // verify assertion
  expect(connecttoanadviserbutton).toBeDefined()
});

test("View FAQs click expand button", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //view faqs dropdown
  await page.waitForSelector('.grid > .relative > #faq-accordion-0 > .flex > .mdi-icon')
  await page.click('.grid > .relative > #faq-accordion-0 > .flex > .mdi-icon')


  // assertion
  const faqsdropdown = await page.locator('.MuiGrid-root > #faq-accordion-0 > #panel0-header > .MuiAccordionSummary-expandIconWrapper > .MuiSvgIcon-root').count()

  // verify assertion
  expect(faqsdropdown).toBeDefined()
});


test("Click See All Used Cars button should redirect to All Used Cars Page ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  // see all used cars button
  await page.waitForSelector('#button-see-all-used-cars', { timeout: 5000 })
  await page.click('#button-see-all-used-cars')

  // assertion
  const AllUsedCarsButton = await page.locator('#button-see-all-used-cars').count()

  // verify assertion
  expect(AllUsedCarsButton).toBeDefined()
});


test("View Our Locations Top Warehouses", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Our Locations
  await page.waitForSelector('body > #__next > .mt-20 > .my-20 > .mb-8')
  await page.click('body > #__next > .mt-20 > .my-20 > .mb-8')

  //Top Warehouses
  await page.waitForSelector('#locations-tab-warehouses')
  await page.click('#locations-tab-warehouses')

  // assertion
  const Topwarehouses = await page.locator('#locations-tab-warehouses').count()

  // verify assertion
  expect(Topwarehouses).toBeDefined()
});


test("Select General Trias Cavite Top Warehouses should redirect to Gen Trias Page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Our Locations
  await page.waitForSelector('body > #__next > .mt-20 > .my-20 > .mb-8')
  await page.click('body > #__next > .mt-20 > .my-20 > .mb-8')

  //Top Warehouses
  await page.waitForSelector('#locations-tab-warehouses')
  await page.click('#locations-tab-warehouses')

  //Gen Trias
  await page.waitForSelector('.mt-20 > .my-20 > .mt-10 > #top-warehouses-gen-trias > .w-fit:nth-child(1)')
  await page.click('.mt-20 > .my-20 > .mt-10 > #top-warehouses-gen-trias > .w-fit:nth-child(1)')

  // assertion
  const GenTrias = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy').count()

  // verify assertion
  expect(GenTrias).toBeDefined()
});

test("View Our Locations Top Cities", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Our Locations
  await page.waitForSelector('body > #__next > .mt-20 > .my-20 > .mb-8')
  await page.click('body > #__next > .mt-20 > .my-20 > .mb-8')

  //Top Cities
  await page.waitForSelector('#locations-tab-cities')
  await page.click('#locations-tab-cities')

  // assertion
  const Topcities = await page.locator('#locations-tab-cities').count()

  // verify assertion
  expect(Topcities).toBeDefined()
});

